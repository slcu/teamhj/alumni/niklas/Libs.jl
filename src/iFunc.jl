function iFunc(outputfunction::Function, widgets::AbstractArray, args...;
            layout=x->display.(collect(Iterators.flatten(x))),
            #layout::Function=default_widget_layout,
            kwargs...
            )
    layout(widgets)

    # idxs = cumsum(length.(widgets))
    # flatwidgets = collect(Iterators.flatten(widgets))
    flatwidgets, idxs = packsignals(widgets...)
    foreach((x...)->outputfunction(args..., unpacksignals(x, idxs)...; kwargs...), signal.(flatwidgets)...)

end


function packsignals(x...)
    x = [i isa AbstractArray ? i : [i] for i in x]
    idxs = cumsum(length.(x))
    flatwidgets = collect(Iterators.flatten(x))
    return flatwidgets, idxs
end

function unpacksignals(x, idxs)
    result = Vector(length(idxs))
    iprev = 0
    for (j, i) in enumerate(idxs)
        i - iprev == 1 ? result[j] = x[i] : result[j] = collect(x[iprev+1:i])
        iprev = i
    end
    return result
end

"""
    get_de_widgets(ode)

return a list of widgets, suitable for passing their signal to plotDE.

The order of the widgets in the list is important since it determines the order
in which arguments is passed to the function downstream of iFunc().
"""
function get_de_widgets(ode::ODEType;
    simulators=true,
    parameters=true,
    param_orientation="horizontal",
    variables = true,
    tstop = true,
    ylimit = true,
    relative_u0 = true,
    u0 = fill(1., length(ode.syms)),
    u0_orientation = "horizontal",
    cont_update = false,
    params = fill(1., length(ode.params))
    )

    widgets = Vector{Array{Interact.InputWidget,1}}()

    simulators && push!(widgets, [selection([ode_plot!, sde_plot!, sde_mc_plot!, sensitivity_plot!], multi=true, orientation="horizontal")])
    #parameters && push!(widgets, [selection_slider(round.(logspace(-3,3,101), 3), label=latexify("$p"), value=getfield(ode, p), orientation=param_orientation, continuous_update=cont_update) for p in ode.params])
    parameters && push!(widgets,
        [
            selection_slider(round.(logspace(-3,3,101), 3),
                label=latexify("$p"),
                value=params[i],
                orientation=param_orientation,
                continuous_update=cont_update)
            for (i, p) in enumerate(ode.params)
        ])
    relative_u0 && push!(widgets, [selection_slider(round.(logspace(-3,3,101), 3), label=latexify("$(u)_0/$(u)_eq"), value=u0[i], orientation=u0_orientation, continuous_update=cont_update) for (i, u) in enumerate(ode.syms)])
    ylimit && push!(widgets, [checkbox(label=latexify("y_min = 0"), value=true)])
    variables && push!(widgets, [selection(ode.syms; multi=true)])
    tstop && push!(widgets, [slider(5.:5.:100., label=latexify("t_stop"), value=10., continuous_update=cont_update)])

    return widgets
end



"""
    display_layout(widgets)

An example of what a layout function should look like.
Not in use.
"""
function display_layout(widgets)
    display(hbox(vbox(w[1]...), vbox(Iterators.flatten(w[2:4])...), vbox(Iterators.flatten(w[5:end])...)))
end

"""
    default_widget_layout()

display widgets in a layout which is usually pretty good for plotDE.
"""
function default_widget_layout(widgets)
    display(hbox(vbox(widgets[2]...), vbox(widgets[1]..., Iterators.flatten(widgets[3:end])...)))
end

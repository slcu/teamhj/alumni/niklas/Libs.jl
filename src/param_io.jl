

"""
    append_param!(fname, ode, params, metadata)

Add param set to file without making duplicates.

The param sets are stored as a matrix in a dict. The dict key is determined by
the ode type. One can thus safely use the same file for multiple different ODEs
without mixing them up.
"""
function append_param!(fname::String, ode::ODEType,
        ode_param::Vector{T}, args::Vector{T}...) where T <: Number

    if length(ode.params) != length(ode_param)
        error("Aborting append_param!, parameter size does not match ode.")
    end
    name = string(typeof(ode))
    param = vcat(ode_param, args...)

    ## keep old params if they exist
    if isfile(fname)
        filecontent = load(fname)
        if haskey(filecontent, name)
            old_param = filecontent[name]::Matrix{T}
            @assert size(old_param, 1) == size(param, 1) "The input you have given for saving does not have the same number of rows as the pre-existing Matrix."
            param = hcat(old_param, param)
        else
            param = hcat(param)
        end

        ## Remove duplicate param sets
        param = unique(param, 2)
        filecontent[name] = param

        JLD.save(fname, filecontent; compatible=true)
    else
        JLD.save(fname, name, hcat(param); compatible=true)
    end
end


function remove_param!(fname::String, ode::ODEType,
        param_to_remove::Vector{T}) where T <: Number

    name = string(typeof(ode))

    if isfile(fname)
        filecontent = load(fname)
        if haskey(filecontent, name)
            param = filecontent[name]::Matrix{T}
            keep_index = [param[:,i] != param_to_remove for i in 1:size(param,2)]
            all(keep_index) && warn("No parameter set to remove")
            param = param[:,keep_index]
            filecontent[name] = param
            save(fname, filecontent; compatible=true)
            any(.!keep_index) && warn("Removed param set $param_to_remove")
        else
            error("No parameters exists for this ode.")
        end
    else
        error("File does not exist.")
    end
end

"""
    get_params(fname, ode)

Get a matrix of parameters from the file fname, with params belonging to ode.
"""
function get_params(fname::String, ode::ODEType)
    name = string(typeof(ode))
    if isfile(fname)
        filecontent = load(fname)
        if !haskey(filecontent, name)
            error("File does not contain any entries for this ODE type.")
        end
    else
        error("File does not exist.")
    end
    M = load(fname, name)
    if size(M, 1) == length(ode.params)
        return (M[1:length(ode.params), :], [])
    else
        return (M[1:length(ode.params), :], M[length(ode.params)+1:end, :])
    end
end

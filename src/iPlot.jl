"""
    `iPlot()`

Interactively plot a differential eqation.

Deals with generating widgets and passing signals to the simulation/plotting
function.

input:
1. ParameterizedFunction or array thereof.
2. Noise function (may be ignored by the simulators)
3. Simulator or array thereof.

The simulators are user-supplied functions which define how the DE should be
solved and plotted. All such functions must be of the form:

`simulator!(plt::Plot, ode, noise, u0::Array, tspan::Tuple, plotkwargs::Dict)`

simulator should add the resulting plot to plt using plot!(plt, ...).
"""
function iPlot end

iPlot(ode; kwargs...) = iPlot(ode, (t,u,du)->du.= 0.1 .* sqrt.(u); kwargs...)
iPlot(ode::AbstractParameterizedFunction, noise; kwargs...) = iPlot([ode], [noise]; kwargs...)
function iPlot(odearray::AbstractArray, args...; kwargs...)
    display(latexalign(odearray))
    iFunc(plotDE, get_de_widgets(odearray[1]), odearray, args...; kwargs...)
end


#=
iPlot(ode::AbstractParameterizedFunction, args...) = iPlot([ode], args...)


iPlot(ode::AbstractParameterizedFunction, noise::Function, simulator::Function) = iPlot(ode, noise, [simulator])

iPlot(ode::AbstractParameterizedFunction, noise::Function) = iPlot([ode], noise)
iPlot(ode::AbstractArray, noise::Function) = iPlot(ode, noise, [ode_plot!, sde_plot!, sde_mc_plot!, sensitivity_plot!])
iPlot(ode::AbstractParameterizedFunction) = iPlot([ode])
iPlot(odearray::AbstractArray) = iPlot(odearray, (t,u,du) -> du .= 0.10 .* sqrt.(u))


function iPlot(odearray::AbstractArray; noise=(t,u,du)->(du.=0.1*u), simulators=[ode_plot!,sde_plot!])
    iPlot(odearray, noise, simulators)
end

function iPlot(odearray::AbstractArray, noise::Function, simulators::AbstractArray)
    ode = odearray[1]

    # create widgets #
    cont_update = false
    parameters = [slider(-3:0.1:3, label=latexify("$p"), value=log10(getfield(ode, p)), continuous_update=cont_update) for p in ode.params]
    plotvars = [selection(ode.syms; multi=true)][simulators, get_de_widgets(f)...]
    tstop = [slider(5.:5.:100., label=latexify("t_stop"), value=10., continuous_update=cont_update)]
    ylimit = [checkbox(label=latexify("y_min = 0"), value=true)]
    relative_u0 = [slider(-2.:.1:2, label=latexify("$(u)_0/$(u)_eq"), continuous_update=cont_update) for u in ode.syms]
    simulator_selection = [selection(simulators, multi=true, orientation="horizontal")]

    display(hbox(vbox(parameters...), vbox(plotvars..., simulator_selection..., ylimit..., tstop..., relative_u0...)))

    nr_p = length(parameters)
    nr_u = length(relative_u0)

    display(latexalign(ode))

    map((x...)-> plotDE(odearray, noise, x[1],
            10. .^collect(x[2:nr_p+1]),
            10. .^collect(x[nr_p+2:nr_p+nr_u+1]),
            x[end-2:end]...
        ),
        signal.(simulator_selection)...,
        signal.(parameters)...,
        signal.(relative_u0)...,
        signal.(ylimit)...,
        signal.(plotvars)...,
        signal.(tstop)...
        )
end
=#

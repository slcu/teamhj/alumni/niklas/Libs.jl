function ode_sim(ode, u0::AbstractArray, tspan::Tuple, param::AbstractArray; callback=nothing, kwargs...)
    prob = ODEProblem(ode, u0, tspan, param)
    # sol = solve(prob, Rodas5(); force_dtmin=true)
    sol = solve(prob, Rosenbrock23(); callback=callback, force_dtmin=true)
    return sol
end

function ode_sim(ode, noise::Function, u0::AbstractArray, tspan::Tuple, param::AbstractArray; kwargs...)
    ode_sim(ode, u0, tspan, param; kwargs...)
end


isnegative(u, t, i) = any(u .< 0)
function retake_step!(i)
    i.u .= i.uprev
    i.t = copy(i.tprev)
end

retake_negative = DiscreteCallback(isnegative, retake_step!)

function sde_sim(rn::AbstractReactionNetwork, u0::AbstractArray, tspan::Tuple, param::AbstractArray; callback=nothing)
    prob = SDEProblem(rn, u0, tspan, param; callback=retake_negative)
    sol = solve(prob) #, delta=1e1)
    sol
end

function sde_sim(ode, noise, u0::AbstractArray, tspan::Tuple, param::AbstractArray; callback=nothing)
    callback = CallbackSet(callback, retake_negative)
    prob = SDEProblem(ode, noise, u0, tspan, param)
    sol = solve(prob; callback=callback) #, delta=1e1)
    sol
end

function ode_multi_param_sim(ode, noise::Function, args...)
    ode_multi_param_sim(ode, args...)
end


function ode_multi_param_sim(ode, u0::AbstractArray{T}, tspan, param; kwargs...) where T <: Number
    ode_multi_param_sim(ode, repmat(u0, size(param, 2)), tspan, param; kwargs...)
end
function ode_multi_param_sim(ode, u0::AbstractArray{T}, args...; kwargs...) where T <: AbstractArray
    ode_multi_param_sim(ode, hcat(u0...), args...; kwargs...)
end
function ode_multi_param_sim(ode, u0::AbstractMatrix, tspan, param::AbstractArray{T}; kwargs...) where T <: AbstractArray
    ode_multi_param_sim(ode, u0, tspan, hcat(param...); kwargs...)
end

function ode_multi_param_sim(ode, u0::AbstractMatrix, tspan, param::AbstractMatrix; callback=nothing)
    prob = ODEProblem(ode, fill(1., length(ode.syms)), tspan, fill(1., length(ode.params)))
    #prob_func = generate_ode_multi_param_problem(u0, param)
    monte_prob = MonteCarloProblem(prob; prob_func = (x...) -> ode_multi_param_problem(u0, param, x...))
    sol = solve(monte_prob, Rosenbrock23(), callback=callback, num_monte=size(param,2))
    #sol = solve(monte_prob; num_monte=2)
    #summ = MonteCarloSummary(sol,linspace(tspan..., 100))
    return sol
end

function ode_multi_param_problem(u0, param, prob, i ,repeat)
    prob.u0 .= u0[:,i]
    prob.p .= param[:,i]
    prob
end

sde_mc_sim(rn::AbstractReactionNetwork, u0::AbstractArray, args...) = sde_mc_sim(rn, rn.g, u0, args...)

function sde_mc_sim(ode, noise, u0, tspan, param::AbstractArray; callback=nothing)
    prob = SDEProblem(ode, noise, fill(1., length(ode.syms)), tspan, param)
    monte_prob = MonteCarloProblem(prob; prob_func = (x...)->assign_u0(u0, x...))
    sol = solve(monte_prob, callback=callback, num_monte=10, delta=1e1)
    summ = MonteCarloSummary(sol,linspace(tspan..., 100))
    return summ
end

function assign_u0(u0::Vector{Vector{T}}, prob, i, repeat) where T<:Number
    prob.u0 .= u0[i]
    prob
end

function assign_u0(u0::Vector{T}, prob, i, repeat) where T<:Number
    prob.u0 .= u0
    prob
end

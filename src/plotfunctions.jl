
"""
    plotPA(ode; kwargs...)

Plot ode trajectories following a set of step-changes in its first parameter.
Returns a plot.

### kwargs (= default)
- param = []
- inputChanges = [[3.0, 1.0],[10.,10.]]
- plotVars = [:y]
- u0 = []
- transformParam = false
"""


plotPA(ode::ODEType, args...; kwargs...) = plotPA!(plot(), ode, args...; kwargs...)


function plotPA!(plt, ode::ODEType,
        simulators::Array, param, relative_u0, ylimit, plotVars, tstop, args...;
    inputChanges=[[3.0, 1.0],[10.,10.]])
    plotPA!(plt, ode, simulators, param, plotVars, ylimit, plotVars; inputChanges=inputChanges)
end



function plotPA!(plt, ode::ODEType,
    simulators::AbstractArray, args...; kwargs...)
    plotPA!(plt, ode, (du, u, p, t) -> (du .= 0.), simulators, args...; kwargs... )
end

function plotPA!(plt, ode::ODEType;
    noise::Function = (du, u, p, t) -> (du .= 0.),
    simulators::Array = [Libs.ode_plot!],
    param::AbstractArray = fill(1., length(ode.params)),
    ylimit = true,
    plotVars = [:y],
    kwargs...)

    plotPA!(plt, ode::ODEType, noise::Function,
        simulators::Array, param::AbstractArray, ylimit, plotVars;
        kwargs...)
end

function plotPA!(plt, ode::ODEType, noise::Function,
    simulators::Array, param::AbstractArray, ylimit, plotVars;
    inputChanges::Array{Array{Float64,1},1} = [[3.0, 1.0],[10.,10.]],
    transformParam::Bool = false, kwargs...)
#function plotPA(ode; param=[], inputChanges=[[3.0, 1.0],[10.,10.]],
        #plotVars=[:y], u0=[], transformParam=false, kwargs...)
    transformParam && (param = paramTransformation(param))
    #isempty(param) && (param = param_values(ode))
    plt = plot(layout=(2,1),
        #xtick=false,
        #ytick=false,
        legend=false,
        size=(300,200),
        leftmargin=30,
        link=:x,
        lw=3,
        grid=false
        )
    linewidth=3

    # equlibriate
    #if isempty(u0)
    u0 = ones(length(ode.syms))
    param[1]=inputChanges[1][2]
    #u0 = findOdeEq(ode, u0, param)
    eq = run_to_eq(ode, u0, param)
    #end


    plotkwargs = Dict( :vars=>plotVars, :color=>:blue, :lw=>linewidth )
    for simulator! in simulators
        tspan = (0.0, 0.0)
        u0 = copy(eq)
        for input in inputChanges
            param[1] = input[2]
            tspan = (tspan[2], input[1])
            u0 = simulator!(plt.subplots[2], ode, noise, u0, param, tspan, plotkwargs)
        end
    end

    ### format for subplot of intput changes.
    inputData = [[],[]]
    tspan = (0.0, 0.0)
    for input in inputChanges
        param[1] = input[2]
        tspan = (tspan[2], input[1])
        push!(inputData[1], tspan[1])
        push!(inputData[2], param[1])
        push!(inputData[1], tspan[2])
        push!(inputData[2], param[1])
    end

    plot!(inputData..., subplot=1, color=:red, lw=linewidth)
    plot!(subplot=1, ylabel="Input", ylim=[0.,-Inf])
    plot!(subplot=2, ylabel="Output", xlabel="Time", xlim=[0.,-Inf])
    ylimit && plot!(subplot=2, ylim = [0., -Inf])
    plot!()
end


function runOde(ode, u0, tspan, param)

    prob = ODEProblem(ode, u0, tspan, param)
    sol = solve(prob, Rosenbrock23())
    return sol
end

function findOdeEq(ode, u0, param)
    prob = SteadyStateProblem(ode, u0, param)
    sol = solve(prob)
    return sol.u
end

function paramTransformation(param)
    [10^p for p in param]
end
